describe('Testing the index page', function() {
    it('Visit index and search a User', function() {
        //Arrange - setup initial app state
        // - visit a web page
        // - query for an element
        cy.visit('http://localhost:8080')
        //Act - take an action
        // - interact with that element
        cy.wait(1000)
        cy.get('input').type('getify')
        cy.wait(1000)
        cy.get('input').type('{enter}')
        
        //Assert - make an assertion
        // - make an assertion about page content
        cy.url().should('include', '/user/getify')
    })
})