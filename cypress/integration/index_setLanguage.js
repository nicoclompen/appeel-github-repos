describe('Testing the index page', function() {
    it('Visit index and select language nl', function() {
        //Arrange - setup initial app state
        // - visit a web page
        // - query for an element
        cy.visit('http://localhost:8080')
        //Act - take an action
        // - interact with that element
        cy.wait(1000)
        cy.get('select').select('NL')
        
        //Assert - make an assertion
        // - make an assertion about page content
        .should('have.value', 'nl')
    })
})