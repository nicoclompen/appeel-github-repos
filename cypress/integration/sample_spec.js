describe('My first test', function() {
    it('Visit the index page', function() {
        //Arrange - setup initial app state
        // - visit a web page
        // - query for an element
        cy.visit('https://example.cypress.io')
        cy.pause()
        //Act - take an action
        // - interact with that element
        cy.contains('type').click()

        //Assert - make an assertion
        // - make an assertion about page content
        cy.url()
            .should('include', '/commands/actions')

        cy.get('.action-email')
            .type('fake@email.com')
            .should('have.value', 'fake@email.com')
    })
})