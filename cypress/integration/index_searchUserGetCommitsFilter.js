describe('Testing the index page', function() {
    it('Search a User and check commits', function() {
        //Arrange - setup initial app state
        // - visit a web page
        // - query for an element
        cy.visit('http://localhost:8080')
        //Act - take an action
        // - interact with that element
        cy.wait(1000)
        cy.get('input').type('getify')
        cy.wait(1000)
        cy.get('input').type('{enter}')
        cy.wait(1000)
        // cy.get('a[href*="/user/getify/repo/asyncGate.js"]').click()
        cy.get('.repo').each((repobody, index, $list) => {
            if (index === 1) {
                cy.wrap(repobody).contains('Commits').click()
                cy.wait(1000)
            }
        })
        cy.wait(1000)
        cy.get('.input').type('initial readme')
        
        //Assert - make an assertion
        // - make an assertion about page content
        cy.url().should('include', '/user/getify/repo/asyncGate.js')
    })
})