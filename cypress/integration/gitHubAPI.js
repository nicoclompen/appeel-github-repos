describe('Testing the github API calls', function() {
    it('test all gitHub APIs needed for this app to work', function() {
        const username = 'getify'
        const reponame = 'asynquence'
        const pageNumber = 1

        //Testing whether the backend API is running or not
        //Using a selfmade command for easy reusing purposes
        cy.githubRequest(`/repos/${username}/${reponame}/commits?per_page=20&page=${pageNumber}`)
        cy.githubRequest(`/search/users?q=${username}`)
        cy.githubRequest(`/users/${username}`)
        cy.githubRequest(`/users/${username}/repos`)
        cy.githubRequest(`/repos/${username}/${reponame}`)

        // cy.request(Cypress.env('api_server') + `/repos/${username}/${reponame}/commits?per_page=20&page=${pageNumber}`).then((resp) => expect(resp.status).to.eq(200))
        // cy.request(Cypress.env('api_server') + `/search/users?q=${username}`).then((resp) => expect(resp.status).to.eq(200))
        // cy.request(Cypress.env('api_server') + `/users/${username}`).then((resp) => expect(resp.status).to.eq(200))
        // cy.request(Cypress.env('api_server') + `/users/${username}/repos`).then((resp) => expect(resp.status).to.eq(200))
        // cy.request(Cypress.env('api_server') + `/repos/${username}/${reponame}`).then((resp) => expect(resp.status).to.eq(200))
    })
})