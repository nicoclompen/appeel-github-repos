module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'nl',
      fallbackLocale: 'nl',
      localeDir: '/core/locales',
      enableInSFC: true
    }
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "@/core/scss/_reset.scss";
                      @import "@/core/scss/_variables.scss";
                      @import "@/core/scss/_styles.scss";`,
      },
    },
  }
}