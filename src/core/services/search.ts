import axios from "axios";
axios.defaults.params = {}
axios.defaults.params["access_token"] = process.env.VUE_APP_ACCESS_TOKEN

export default class SearchService {
    async getSearchByUserName(username:String) {
        return await axios
        .get(process.env.VUE_APP_API_BASE_URL_DATA + `/search/users?q=${username}`)
        .then((response: any) => {
            return response.data.items
        })
    }
}