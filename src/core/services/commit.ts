import axios from "axios";
axios.defaults.params = {}
axios.defaults.params['access_token'] = process.env.VUE_APP_ACCESS_TOKEN

export default class RepositoryService {
    async getCommitsByUserRepoAndPage(username:String, reponame:String, pageNumber:String) {
        return await axios
        .get(process.env.VUE_APP_API_BASE_URL_DATA + `/repos/${username}/${reponame}/commits?per_page=20&page=${pageNumber}`)
        .then((response: any) => {
            return response.data
        })
    }
}