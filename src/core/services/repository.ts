import axios from "axios";
axios.defaults.params = {}
axios.defaults.params["access_token"] = process.env.VUE_APP_ACCESS_TOKEN

export default class RepositoryService {
    async getUserByUserName(username:String) {
        return await axios
        .get(process.env.VUE_APP_API_BASE_URL_DATA + `/users/${username}`)
        .then((response: any) => {
            return response.data
        })
    }

    async getRepositoriesByUserName(username:String) {
        return await axios
        .get(process.env.VUE_APP_API_BASE_URL_DATA + `/users/${username}/repos`)
        .then((response: any) => {
            return response.data
        })
    }

    async getRepositoryByUserAndRepoName(username:String, reponame:String) {
        return await axios
        .get(process.env.VUE_APP_API_BASE_URL_DATA + `/repos/${username}/${reponame}`)
        .then((response: any) => {
            return response.data
        })
    }
}