import iTree from '@/core/models/iTree'
import iVerification from '@/core/models/iVerification'
import iCommitter from '@/core/models/iCommitter'
import iAuthor from '@/core/models/iAuthor'

export default interface iCommitDetails {
    author: iAuthor;
    committer: iCommitter;
    message: string;
    tree: iTree;
    url: string;
    comment_count: number;
    verification: iVerification;
}