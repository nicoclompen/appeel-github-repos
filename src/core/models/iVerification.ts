export default interface iVerification {
    verified: boolean;
    reason: string;
    signature: string;
    payload: string;
}