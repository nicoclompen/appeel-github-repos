import iParent from '@/core/models/iParent'
import iCommitter from '@/core/models/iCommitter'
import iCommitDetails from '@/core/models/iCommitDetails'
import iAuthor from '@/core/models/iAuthor'

export default interface iCommit {
    sha: string;
    node_id: string;
    commit: iCommitDetails;
    url: string;
    html_url: string;
    comments_url: string;
    author: iAuthor;
    committer: iCommitter;
    parents: iParent[];
}