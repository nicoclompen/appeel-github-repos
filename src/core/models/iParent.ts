export default interface iParent {
    sha: string;
    url: string;
    html_url: string;
}