import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import userRoutes from '@/modules/user/routes'

Vue.use(VueRouter)

let allRoutes: Array<RouteConfig> = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   // route level code-splitting
  //   // this generates a separate chunk (home.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "home" */ '@/modules/user/pages/Home.vue')
  // }
]

allRoutes = allRoutes.concat(userRoutes);

const routes = allRoutes;
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
