const userRoutes = [
    {
        path: "/",
        name: "user-index",
        component: () =>
            import(
                /* webpackChunkName: "user-index" */ "@/modules/user/pages/Index.vue"
            )
    },
    {
        path: "/user/:userName",
        name: "user-repos",
        component: () =>
            import(
                /* webpackChunkName: "user-repos" */ "@/modules/user/pages/userRepositories.vue"
            )
    },
    {
        path: "/user/:userName/repo/:repoName",
        name: "user-repo",
        component: () =>
            import(
                /* webpackChunkName: "user-repo" */ "@/modules/user/pages/userRepository.vue"
            )
    },
];

export default userRoutes;