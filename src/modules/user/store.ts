import Vue from 'vue'
import Vuex from 'vuex'
import { Module } from 'vuex'

import SearchService from '../../core/services/search'
import iUser from '@/core/models/iUser.ts'
const searchService = new SearchService()

import RepoService from '../../core/services/repository'
import iRepo from '@/core/models/iRepo.ts'
const repoService = new RepoService()

import CommitService from '../../core/services/commit'
import iCommit from '@/core/models/iCommit'
const commitService = new CommitService()

const userModule: Module<any, any> = {
  state: {
      user: {} as iUser,
      users: [] as iUser[],
      repos: [] as iRepo[],
      repo: {} as iRepo,
      commits: [] as iCommit[]
  },
  getters: {
      filteredCommits(state){
          return (filterValue:String) => {
            let arrayToFilter = new Array(state.commits)[0]
                if(filterValue == null){
                    return state.commits
                } else {
                    return arrayToFilter.filter((filterItem:any) => filterItem.commit.message.includes(filterValue))
                }
          }
      }
  },
  mutations: {
    setUserModuleState(state, item){
        Vue.set(state, item.key, item.value)
    },
    concatUserModuleState(state, item){
        let oldArray = state[item.key]
        Vue.set(state, item.key, oldArray.concat(item.value))
    },
    orderUserModuleStateBy(state, item){
        let arrayToOrder = state[item.key]

        if(typeof arrayToOrder[0][item.value] === 'string'){
            arrayToOrder.sort((a:any, b:any) => {
                return a[item.value].localeCompare(b[item.value]);
            });
        } else if (typeof arrayToOrder[0][item.value] === 'number') {
            arrayToOrder.sort((a:any, b:any) => {
                return b[item.value] - a[item.value];
            });
        }

        Vue.set(state, item.key, arrayToOrder)
    }
  },
  actions: {
    async getSetUserByUserName(context, username: String){
        return await repoService.getUserByUserName(username).then(response => {
            context.commit('setUserModuleState', {key: 'user', value: response})
            return response
        })
    },

    async getSetUsersByUserName(context, username: String){
        return await searchService.getSearchByUserName(username).then(response => {
            context.commit('setUserModuleState', {key: 'users', value: response})
            return response
        })
    },

    async getSetReposByUsername(context, username: String){
        return await repoService.getRepositoriesByUserName(username).then(response => {
            context.commit('setUserModuleState', {key: 'repos', value: response})
            return response
        })
    },

    async getSetRepoByUserAndRepoName(context, { username, reponame }){
        return await repoService.getRepositoryByUserAndRepoName(username, reponame).then(response => {
            context.commit('setUserModuleState', {key: 'repo', value: response})
            return response
        })
    },

    async getSetCommitsByUserRepoAndPage(context, { username, reponame, page }){
        return await commitService.getCommitsByUserRepoAndPage(username, reponame, page).then(response => {
            if(response.length != 0)
                context.commit('concatUserModuleState', {key: 'commits', value: response})
            return response
        })
    }
  }
}

export default userModule;