# Appeel Github Repos

An awesome project for searching Github users and displaying their repositories & commits

* Before you begin you must make a duplicate of the '.env.example' file and rename it to '.env'
* Open the following url and create an access_token to place inside your newly created env file https://github.com/settings/tokens/new
* Paste the access_token inside the env file (VUE_APP_ACCESS_TOKEN)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve

While running the project, try to search for the user 'getify' and choose for the 'Asynquence' Repo (third option) to try out the **infinite scrolling feature**

```

### Compiles and minifies for production
```
npm build
```

### To run e2e tests
```
npm run cypress:open
```

### To run unit tests
```
npm run unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Reused component
VSelect.vue -> partially reused component from older project to create the easy search and select user feature.

### Used ES6+ features

* Const, to declare a variable that can't and shouldn't change it's value in the future. In Typescript this is strict, in JS not (Used in most files such as userRepository, userRepositories, etc)
* Arrow, another way of writing functions cleaner and shorter. Also no binding of this which is highly usefull in Vue.Js (userRepository, userRepositories, etc)
* Math.random, is used to rapidly generate random numbers between a set min and max. (Used in the generate.ts file for choosing random loadingTexts)
* Includes, to check if a string contains a set of letters or word. (Used in the store.ts file (userModule) to filter on an input field)
