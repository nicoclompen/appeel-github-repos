module.exports = {
    moduleNameMapper: {
        "^vue$": "vue/dist/vue.common.js",
        "/src/(.*)": "<rootDir>/src/$1"
    },
    transform: {
        "\\.tsx?$": "ts-jest",
        "^.+\\.js$": "./node_modules/babel-jest",
        ".*\\.(vue)$": "./node_modules/vue-jest"
    },
    testPathIgnorePatterns : [
        "<rootDir>/cypress/"
    ]
 }