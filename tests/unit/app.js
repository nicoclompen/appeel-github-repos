import Vue from "vue";
import App from "../../src/App.vue";

describe("App.test.js", () => {
  let cmp, vm;

  beforeEach(() => {
    cmp = Vue.extend(App);
    vm = new cmp({
      data: {
        // Replace data value with this fake data
        messages: ["Test"]
      }
    }).$mount(); // Instances and mounts the component
  });

  it('equals messages to ["Test"]', () => {
    expect(vm.messages).toEqual(["Test"]);
  });
});