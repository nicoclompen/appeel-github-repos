import axios from 'axios';
class User {
  static getUserByUserName(username) {
    return axios.get(process.env.VUE_APP_API_BASE_URL_DATA + `/users/${username}`)
    .then(resp => resp.data);
  }
}

export default User;