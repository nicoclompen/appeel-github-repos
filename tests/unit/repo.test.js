import { shallowMount, mount } from '@vue/test-utils'
import repo from '../../src/modules/user/components/userRepositories/repo.vue'
import jest from 'jest-mock';

test('repo', async () => {
  const $t = () => {}
  const store = {
    dispatch: jest.fn().mockImplementationOnce(),
  };
  
  // render the component
  const wrapper = shallowMount(repo, {
      propsData: {
        repo: { id:1, name:"async" },
      },
      mocks:{ $t }
  })

  // assert the error is rendered
  expect(wrapper.text('.repo__header:first-child > p')).toEqual("async")
})