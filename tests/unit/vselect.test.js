import { shallowMount, mount } from '@vue/test-utils'
import VSelect from '../../src/core/components/VSelect.vue'

test('VSelect', async () => {
  const $t = () => {}
  
  // render the component
  const wrapper = mount(VSelect, {
      propsData: {
        url:"getSetUsersByUserName",
        name:"user_id",
        showValue:"login",
      },
      mocks:{ $t }
  })

  // assert the error is rendered
  expect(wrapper.vm.$emit('option:selecting', {action: 'selecting', name: 'user_id', value: 1,}))
  await wrapper.vm.$nextTick()

  // assert the error has gone away
  expect(wrapper.emitted()['option:selecting'][0][0].value).toBe(1)
})