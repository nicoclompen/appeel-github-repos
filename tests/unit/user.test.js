import axios from 'axios';
import User from './user';

jest.mock('axios');

//Testing of a facke service by mocking axios
test('should fetch user by username', () => {
  const user = {login: 'getify'};
  const resp = {data: user};
  axios.get.mockResolvedValue(resp);

  return User.getUserByUserName('getify').then(data => expect(data).toEqual(user));
});