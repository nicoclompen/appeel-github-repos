import { shallowMount, mount } from '@vue/test-utils'
import userRepositories from '../../src/modules/user/pages/userRepositories.vue'
import store from '../../src/core/store'
import jest from 'jest-mock';

test('userRepositories', async () => {
  const $t = () => {}
  const store = {
    dispatch: jest.fn().mockImplementationOnce(),
  };

  beforeEach(() => {
    actions = {
      getSetReposByUsername: jest.fn(),
      getSetUserByUsername: jest.fn()
    }
    store = new Vuex.Store({
      actions
    })
  })
  
  // render the component
  const wrapper = shallowMount(userRepositories, {
      propsData: {
        url:"getSetUsersByUserName",
        name:"user_id",
        showValue:"login",
      },
      computed: {
        repos: () => [ {id: 1, name: "hello"}, {id: 2, name: "hi"} ],
        user: () => ({ id: 1, name: "Getify" })
      },
      mocks:{ $t }
  })

  // assert the error is rendered
  expect(wrapper.findAll('.repo').length).toEqual(2)
  expect(store.dispatch).toHaveBeenCalledWith('getSetReposByUsername', null, {root: true});
  expect(store.dispatch).toHaveBeenCalledWith('getSetUserByUsername', null, {root: true});
})