import { shallowMount, mount } from '@vue/test-utils'
import Dropdown from '../../src/core/components/Dropdown.vue'

test('Dropdown', async () => {
  // render the component
  const wrapper = mount(Dropdown, {
      propsData: {
        placeholder:"EN",
        name:"language",
        options: [{
            key: 'nl',
            value: 'NL'
        },
        {
            key: 'en',
            value: 'EN'
        }],
        optionSelected: 0
      }
  })

  // assert the error is rendered
  expect(wrapper.vm.$emit('updateOption', {name: 'language', value: 'EN'}))
  await wrapper.vm.$nextTick()

  // assert the error has gone away
  expect(wrapper.emitted().updateOption[0][0].value).toBe('EN')
})